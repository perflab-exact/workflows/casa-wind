.PHONY: all

all:
	$(MAKE) -C src/pointAlert
	$(MAKE) -C src/UM_VEL
	$(MAKE) -C src/mvt
	$(MAKE) -C src/netcdf2png

clean:
	$(MAKE) -C src/pointAlert clean
	$(MAKE) -C src/UM_VEL clean
	$(MAKE) -C src/mvt clean
	$(MAKE) -C src/netcdf2png clean