CASA Wind
=============================================================================

**Home**:
  - https://gitlab.com/perflab-exact/workflows

Code taken from pegasus containers.

## pegasus/casa-wind
Container: https://hub.docker.com/r/pegasus/casa-wind \
Container code: https://github.com/pegasus-isi/casa-containers/tree/master/wind \
Workflow: https://github.com/pegasus-isi/casa-wind-workflow


## Dependencies
```
HDF5
NetCDF-C>=4.6
libconfig=1.5
libxml2=2.9.4
jansson=2.7
libpng=1.6.34
```
- setup the correct paths, and `source load_dep.sh` to prepare environment paths
- run `make` to build each program in subdirectories

## Datasets
Dataset is taken from the `testdata` branch of the pegasus workflow
```
git clone https://github.com/pegasus-isi/casa-wind-workflow
cd casa-wind-workflow
git checkout testdata
```

## Run the program
- setup the correct paths, and `source load_dep.sh` to prepare environment paths
- run `make` to build each program in subdirectories
### Input Datasets
- Put all input datas into `$EXPERIMENT_PATH/input`
- Input data should also include `max_wind.png`.
### Time Parameters
- adjust `END_TIME=710` to vary the number of input files, largest input time is `749`.
- Valid `START_TIME` and `END_TIME` values:
```
START_TIME=710 # from hour 10
END_TIME=749 # to hour 49+

or 

START_TIME=7100 # from hour 10 minute 0
END_TIME=7105 # to hour 10 minute 5+

or 

START_TIME=71008 # from hour 10 minute 0 second 8
END_TIME=071355 # to hour 13 minute 5 second 5
```
(check input dataset names for valid time ranges)
### Other Parameters
- modify the below paths for use
```
EXPERIMENT_PATH="/qfs/projects/oddite/tang584/casa_wind_runs"
OUTPUT_PATH="$EXPERIMENT_PATH/output"
```
### Correcness
You can check output under test/example_logs for program correctness.

## Note:
The workflow in test/seq_find.sh does not implement the last step `pointalert`, which does not use or output any NetCDF files.
