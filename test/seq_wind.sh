#!/bin/bash

# set -x
EXPERIMENT_PATH="/qfs/projects/oddite/tang584/casa_wind_runs"
PROGRAM_PATH="../src"
LOG_PATH="./example_logs"
mkdir -p $LOG_PATH

INPUT_PATH="$EXPERIMENT_PATH/input"
OUTPUT_PATH="$EXPERIMENT_PATH/output"
mkdir -p $OUTPUT_PATH/

EXP_CONF_MVT="./mvt_example.txt"
CONF_MVT_NAME="mvt_config.txt"
EXP_CONF_UV="./UM_VEL_example.txt"
CONF_UV_NAME="UM_VEL_config.txt"
INPUT_N2P="$INPUT_PATH/max_wind.png"
cp ./max_wind.png $INPUT_N2P
# EXP_CONF_PA="./pointAlert_example.txt"
# CONF_PA_NAME="pointAlert_config.txt"

EXP_DATE="20170329" # fixed variable
# Time split into batch of 1 minute (or is it hourly?)
START_TIME=710 # earliest starttime
END_TIME=710 # latest endtime
# END_TIME=749 # latest endtime

DATA_FOLDER="UM_VEL" # fixed variable
NETCDF_DATA_PATH="$OUTPUT_PATH/$DATA_FOLDER"
mkdir -p $NETCDF_DATA_PATH

# get time sequence file names
NETCDF_ZIP_FILES=()

current_time=$START_TIME
while (( current_time <= END_TIME )); do
time_seq+=("$current_time")
current_time=$(($current_time + 1))
done

echo "Analyzing Below Input Files:"
for data_time in "${time_seq[@]}"; do
    for file in "$INPUT_PATH"/*$EXP_DATE-0$data_time*.gz; do
        echo "$file"
        NETCDF_ZIP_FILES+=("$file")
    done
done
echo "Total ${#NETCDF_ZIP_FILES[@]} files"




PREP_CONFIGS () {
    # rm -rf ${FUNCNAME[0]}.log

    # Prepare UM_VEL config
    CONF_UV="$NETCDF_DATA_PATH/$CONF_UV_NAME"
    cp $EXP_CONF_UV $CONF_UV
    sed -i "s|NETCDF_DATA_PATH|$NETCDF_DATA_PATH|g" $CONF_UV
    export UM_VEL="$NETCDF_DATA_PATH"

    # Prepare mvt config
    CONF_MVT="$NETCDF_DATA_PATH/$CONF_MVT_NAME"
    cp $EXP_CONF_MVT $CONF_MVT
    sed -i "s|CONF_MVT|$CONF_MVT|g" $CONF_MVT
    sed -i "s|NETCDF_DATA_PATH|$NETCDF_DATA_PATH|g" $CONF_MVT
    export MVTHOME="$NETCDF_DATA_PATH"

    echo "Prep configs done ..."
}


UNZIP_DATA () {
    # Prepare folder
    mkdir -p $NETCDF_DATA_PATH
    rm -rf $NETCDF_DATA_PATH/*
    # netcdf_data_files=("$@")  # Accept the array as an argument

    start_time=$(($(date +%s%N)/1000000))
    # Stage 1
    echo "UNZIP_DATA: Total ${#NETCDF_ZIP_FILES[@]} files"
    
    for file in "${NETCDF_ZIP_FILES[@]}"; do
        echo "Unzipping $file"
        unzipped_file=$(basename "$file" .gz)
        # unzipped_file=$(basename "$file" .netcdf.gz)
        # unzipped_file="${unzipped_file}.nc"
        set -x
        gunzip -c $file > $NETCDF_DATA_PATH/$unzipped_file
        set +x
    done

    duration=$(( $(date +%s%N)/1000000 - $start_time))
    echo "${FUNCNAME[0]} done... $duration milliseconds elapsed."
    # ls -l $NETCDF_DATA_PATH/* | grep .nc
}

MAX_VELOCITY () {
    # rm -rf ${FUNCNAME[0]}.log

    start_time=$(($(date +%s%N)/1000000))

    # Stage 2
    input_file_list=""
    
    for file in "$NETCDF_DATA_PATH"/*.netcdf; do
    # for file in "$NETCDF_DATA_PATH"/*.nc; do
        if [[ "$file" != *"MaxVelocity"* ]]; then
            input_file_list+="$file "
        fi
    done
    input_file_list=${input_file_list%%*( )}

    # echo "Input file list: $input_file_list"

    $PROGRAM_PATH/UM_VEL/UM_VEL $input_file_list
    
    duration=$(( $(date +%s%N)/1000000 - $start_time))
    echo "${FUNCNAME[0]} done... $duration milliseconds elapsed." 
}

MVT () {
    input_filename=$(ls $NETCDF_DATA_PATH/*.netcdf | grep "MaxVelocity")
    start_time=$(($(date +%s%N)/1000000))
    # Stage 3.1
    set -x
    $PROGRAM_PATH/mvt/mvt $input_filename
    xet +x
    
    duration=$(( $(date +%s%N)/1000000 - $start_time))
    echo "${FUNCNAME[0]} done... $duration milliseconds elapsed."
}

NETCDF2PNG () {
    #       - -c
    #   - max_wind.png
    #   - -q 235 -z 11.176,38
    #   - -o
    #   - MaxVelocity_20170329-074901.png
    #   - MaxVelocity_20170329-074901.netcdf

    # set -x
    start_time=$(($(date +%s%N)/1000000))
    # Stage 3.2
    input_filename=$(ls $NETCDF_DATA_PATH/*.netcdf | grep "MaxVelocity")
    basename=$(basename "$input_filename" .netcdf)
    output_file="$NETCDF_DATA_PATH/$basename.png"

    # echo $file
    set -x
    $PROGRAM_PATH/netcdf2png/merged_netcdf2png -c $INPUT_N2P \
        -q 235 \
        -z 11.176,38 \
        -o $output_file \
        $input_filename
    set +x

    duration=$(( $(date +%s%N)/1000000 - $start_time))
    echo "${FUNCNAME[0]} done... $duration milliseconds elapsed."
    ls -l $OUTPUT_PATH/* | grep png

}




total_start_time=$(($(date +%s%N)/1000000))

UNZIP_DATA | tee $LOG_PATH/UNZIP_DATA.log
# sleep 2
PREP_CONFIGS
MAX_VELOCITY | tee $LOG_PATH/MAX_VELOCITY.log
MVT | tee $LOG_PATH/MVT.log
NETCDF2PNG | tee $LOG_PATH/NETCDF2PNG.log

total_duration=$(( $(date +%s%N)/1000000 - $total_start_time))
echo "Total time elapsed: $total_duration milliseconds." | tee -a $LOG_PATH/NETCDF2PNG.log


